#!/bin/zsh

GIT_COMMIT=$(git log -1 --format=%h)
docker build -t "cloud-hw-3:${GIT_COMMIT}" --build-arg app_version="${GIT_COMMIT}" src/
docker tag "cloud-hw-3:${GIT_COMMIT}" "cr.yandex/crpt8relsil6a44i0885/cloud-hw-3:${GIT_COMMIT}"
docker push "cr.yandex/crpt8relsil6a44i0885/cloud-hw-3:${GIT_COMMIT}"
